# Business Impact Analyser

This is the Soccrates Business Impact Analyser component repository. There is everything
needed to build the Docker image of the Business Impact Analyser API and GUI.

## Swarm deployment

Deployment expects 3 infrastructure specific files in a folder `../config/bia/`, see the https://ci.tno.nl/gitlab/soccrates/software/config submodule and replace those files with your own. Check the `SOCCRATES_BIA.docx` for information about these files. These files are mounted to the running containers through [docker swarm configs](https://docs.docker.com/engine/swarm/configs/) as declared in the `docker-compose.yml`

Also note that, in order to be able to share a volume, you add the bia label to the node you want to run the `bia-processing` and `bia-backend` containers on. They must reside on the same node in the swarm.

```bash
docker node update --label-add bia=true <node id>
```

```bash
# replace soccrates.xyz with the IP of your swarm manager or your domain name
HOST_IP_OR_DOMAIN=soccrates.xyz DEPLOYMENT_UID=$RANDOM make -B bia
```

## Sub directories

- bia_api:
Contains the _Python_ API of the Business Impact Analyser, made with _fastapi_
and _uvicorn_ librairies.

- bia_gui:
Contains the _JavaScript_ Graphical User Interface of the Business Impact Analyser.

## Local Development

Address and port configuration can be changed at different locations.

### BIA API port

If the BIA api port is changed in the _docker-compose_ yaml file, the following
variables should be updated:

bia_gui/graph-creator.js:

- _biaBackendPort_ (line 540)

For CortexAnalyser :

#### TO DO

Update these files:

- SOCCRATES_IMPACT_ANALYZER.json configurationItems: url
- SOCCRATES_CRITICAL_ANALYZER.json configurationItems: url
- SOCCRATES_IMPACT_RESPONDER.json configurationItems: url

### BIA GUI port

If the BIA GUI address or port are changed in the `docker-compose.yml` yaml file,
the following variables should be updated in `bia_api/app/config.py`:

- _BIA_GUI_ADDRESS_ (line 4)
- _BIA_GUI_PORT_ (line 5)

## Docker image building and deployment

### Build docker Image

`sudo docker build -t bia .`

### Test the docker deployment

#### Run docker image

`sudo docker run -it bia`

#### Get the docker Ip address

`sudo docker ps`

`sudo docker inspect $containerID`

#### Test the APIs

- BIA api : [http://$containerIP:8000]

- BIA graphical user interface: [http://$containerIP:80]

More details on the deployment of the BIA can be found in this document : SOCCRATES_BIA.docx

## BIA testing and validation

You can use the BIA API to test the Business Impact Analyser component: (the API is reachable via [http://0.0.0.0:8002/docs]:

1. Use the "criticality" function in the API to evaluate the assets criticality and visualize the graph (generated from the IMC data):

   - To calculate the criticality of the assets, you need to set the _Threshold_ value (you can define for example `Threshold = 0,1`) or keep the default value. This function displays the UUIDs of the assets whose criticality is higher or equal to the threshold value.
   - To visualize the graph, this function generates an URL ("report_url") to give the SOC operator the possibility to visualize and interact with the graph through the Graphical User Interface. In the graphical interface, to identify the most critical assets, you should press the `Get Criticality` button.

2. Use the "probability" function in the API to calculate the impact of a shock event on the business entities (BFs, BPs and BC):

   - Set the "assetID" variable by putting the UUID of the target asset (The UUID is provided by the IMC). This function calculates the impact on the business entities (by default the "Business Company") : impact probability and critical time.
   - To visualize the graph, this function generates an URL ("report_url"), you can compute the business impact of an external event on the business entities via the "Get Impact Probability" button.

3. You can calculate the critical time using the same function "probability":

   - In the graphical interface, you need to change the graph type from Probability to Temporal.
   - Press the "Get Critical Time" button to compute the critical time. After computation of the critical time, the shortest path will be displayed in dotted on the downtime tolerance graph.

