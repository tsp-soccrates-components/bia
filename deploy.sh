#!/bin/bash

tag=${DOCKER_TAG:-0.8.15}
printf "tag is %s\n" "$tag"

env TAG=${tag} docker stack deploy --prune --with-registry-auth \
    -c docker-compose.yml bia
