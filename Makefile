network:
	docker network create --driver overlay soccrates || true
	docker network create --driver overlay proxy || true

bia: network
	echo bia will be at https://bia.$$HOST_IP_OR_DOMAIN
	bash deploy.sh

configMap:
	kubectl create configmap "{{ .Release.Name }}-bia-model" --namespace="{{ .Release.Namespace }}" --from-file=demo-model -o yaml --dry-run > templates/model.configmap.yaml


