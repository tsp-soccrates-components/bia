---
version: "3.8"

services:

  bia-processing:
    image: ci.tno.nl:4567/soccrates/software/bia/bia-processing:${TAG:-0.8.15}
    logging:
      options:
        max-size: 500m
    env_file:
      - ../config/shared.env
      - ../config/bia.env
      - ../config/act.env
      - ../config/elastic.env
      - ../config/elastic.secrets.env
    environment:
      ACT_LOG_LEVEL: info
      LOGGING_LEVEL: DEBUG
    command: python3 /app/process.py
    networks:
      - soccrates
      - proxy
    volumes:
      - bia_model:/model:rw
    configs:
      - source: assets
        target: /tmp/Assets_UUID.json
      - source: bf-probabilities
        target: /tmp/BF_Prob.csv
      - source: bf-time
        target: /tmp/BF_time.csv
    deploy:
      labels:
        # Portainer
        - "io.portainer.accesscontrol.public"
      resources:
        reservations:
          cpus: '1'
          memory: 6G
        limits:
          cpus: '1'
          memory: 6G
      placement:
        constraints:
          - "node.labels.bia==true"

  bia-backend:
    image: ci.tno.nl:4567/soccrates/software/bia/bia-backend:${TAG:-0.8.15}
    logging:
      options:
        max-size: 500m
    env_file:
      - ../config/shared.env
      - ../config/bia.env
    networks:
      - soccrates
      - proxy
    volumes:
      - bia_model:/model:rw
    configs:
      - source: assets
        target: /tmp/Assets_UUID.json
      - source: bf-probabilities
        target: /tmp/BF_Prob.csv
      - source: bf-time
        target: /tmp/BF_time.csv
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.services.bia-backend.loadbalancer.server.port=80"
        # TLS
        - "traefik.http.routers.bia-backend.tls=true"
        - "traefik.http.routers.bia-backend.rule=Host(`bia-backend.${HOST_IP_OR_DOMAIN}`)"
        - "traefik.http.routers.bia-backend.entrypoints=web-secured"
        # Authelia
        - 'traefik.http.routers.bia-backend.middlewares=authelia@docker'
        # Portainer
        - "io.portainer.accesscontrol.public"
      resources:
        reservations:
          cpus: '1'
          memory: 1G
        limits:
          cpus: '1'
          memory: 1G
      placement:
        constraints:
          - "node.labels.bia==true"

  bia:
    image: ci.tno.nl:4567/soccrates/software/bia/bia:${TAG:-0.8.15}
    logging:
      options:
        max-size: 500m
    env_file:
      - ../config/shared.env
      - ../config/bia.env
    networks:
      - soccrates
      - proxy
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.services.bia.loadbalancer.server.port=80"
        # TLS
        - "traefik.http.routers.bia.tls=true"
        - "traefik.http.routers.bia.rule=Host(`bia.${HOST_IP_OR_DOMAIN}`)"
        - "traefik.http.routers.bia.entrypoints=web-secured"
        # Authelia
        - 'traefik.http.routers.bia.middlewares=authelia@docker'
        # Portainer
        - "io.portainer.accesscontrol.public"
      resources:
        reservations:
          cpus: '1'
          memory: 8G
        limits:
          cpus: '1'
          memory: 8G

volumes:
  bia_model:


configs:
  assets:
    name: assets-${DEPLOYMENT_UID}
    file: ../config/bia/Assets_UUID.json
  bf-probabilities:
    name: bf-probabilities-${DEPLOYMENT_UID}
    file: ../config/bia/BF_Prob.csv
  bf-time:
    name: bf-time-${DEPLOYMENT_UID}
    file: ../config/bia/BF_time.csv

networks:
  soccrates:
    external: true
  proxy:
    external: true
