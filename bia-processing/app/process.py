import BF_matrix
import data_processing
import schedule
import time


def process():
    BF_matrix.main("Assets_UUID.json")
    data_processing.buildModel()


# do an initial run
process()

# and schedule daily runs
schedule.every().day.at("00:01").do(process)
while True:
    schedule.run_pending()
    time.sleep(60)
