import datetime
import logging
import os
from typing import Any, Dict, Iterator, Optional, Text

from elasticsearch import Elasticsearch, exceptions


class ElasticIngestor:
    def __init__(self) -> None:

        if "https_proxy" in os.environ:
            del os.environ["https_proxy"]
        if "HTTPS_PROXY" in os.environ:
            del os.environ["HTTPS_PROXY"]

        self.elastic_host = os.environ.get("ELASTIC_HOST")
        self.elastic_port = os.environ.get("ELASTIC_PORT")
        self.elastic_tls = os.environ.get("ELASTIC_SSL")
        self.elastic_username = os.environ.get("ELASTIC_USER")
        self.elastic_password = os.environ.get("ELASTIC_PASSWORD")
        self.elastic_index = os.environ.get("ELASTIC_INDEX")
        self.elastic_client = self.connect_elastic()

    def connect_elastic(self, timeout: int = 180) -> Elasticsearch:
        logging.debug("Setting up connection to Elastic")
        try:
            if self.elastic_tls:
                return Elasticsearch(
                    f"https://{self.elastic_username}:{self.elastic_password}@"
                    f"{self.elastic_host}:{self.elastic_port}",
                    verify_certs=False,
                    ssl_show_warn=False,
                    timeout=timeout,
                )
            elif self.elastic_username and self.elastic_password:
                return Elasticsearch(
                    f"http://{self.elastic_username}:{self.elastic_password}@"
                    f"{self.elastic_host}:{self.elastic_port}",
                    timeout=timeout,
                )
            else:
                return Elasticsearch(
                    f"http://{self.elastic_host}:{self.elastic_port}", timeout=timeout
                )
        except Exception as error:
            logging.critical(f"Unable to connect to Elasticsearch: {error}")
            return None

    def sql(
        self,
        client: Elasticsearch,
        query: Text,
        time_zone: Text = "Europe/Oslo",
        debug: bool = False,
        cursor: Optional[Text] = None,
    ) -> Any:
        """Elasticsearch SQL query"""

        body = {"query": query, "time_zone": time_zone}

        if cursor:
            body["cursor"] = cursor

        return client.sql.query(body=body)

    def fetch_flows(
        self,
        start_time: datetime.datetime = (
            datetime.datetime.today() - datetime.timedelta(days=1)
        ).replace(hour=0, minute=0, second=0, microsecond=0),
        end_time: datetime.datetime = (
            datetime.datetime.today() - datetime.timedelta(days=1)
        ).replace(hour=23, minute=59, second=59, microsecond=999999),
        query: Optional[Text] = None,
    ) -> Iterator[Dict[Text, Any]]:

        """
        Create composite aggregations and iterator over results
        Exclude UDP flows with no data returned
        Exclude ICMP traffic (not relevant for BIA)
            - Determine mapping of proto (numeric or string)
        """

        mapping = self.get_mapping(self.elastic_client)

        first_index = list(mapping.keys())[0]

        proto_mapping = (
            mapping.get(first_index)
            .get("mappings")
            .get("properties")
            .get("proto")
            .get("type")
        )

        logging.debug(f"Proto is of type {proto_mapping} for index {first_index}")
        proto = "'ICMP' AND NOT \"proto\" = '1'" if proto_mapping == "text" else 1

        range_field: str = "flow.end"

        query = f"""
SELECT "src_ip.keyword",
    "dest_ip.keyword",
    SUM("flow.bytes_toclient") as bytes_in,
    SUM("flow.bytes_toserver") as bytes_out
FROM "{self.elastic_index}"
WHERE
NOT "flow.bytes_toclient" = 0
AND NOT "proto" = {proto}
AND NOT "proto" IS NULL
AND "{range_field}" BETWEEN '{start_time.isoformat()}'
                        AND '{end_time.isoformat()}'
GROUP BY "src_ip.keyword", "dest_ip.keyword"
        """
        logging.debug(f"Query: {query}")

        done = False
        cursor = None

        while not done:
            try:
                search = self.sql(
                    self.elastic_client,
                    query,
                    time_zone=os.environ.get("TZ", "Europe/Oslo"),
                    cursor=cursor,
                )
            except exceptions.RequestError:
                query_without_keyword = f"""
SELECT "src_ip",
    "dest_ip",
    SUM("flow.bytes_toclient") as bytes_in,
    SUM("flow.bytes_toserver") as bytes_out
FROM "{self.elastic_index}"
WHERE
NOT "flow.bytes_toclient" = 0
AND NOT "proto" = {proto}
AND NOT "proto" IS NULL
AND "{range_field}" BETWEEN '{start_time.isoformat()}'
                        AND '{end_time.isoformat()}'
GROUP BY "src_ip", "dest_ip"
"""
                logging.debug(f"query failed. retrying without `.keyword`. Query: {query}")
                search = self.sql(
                    self.elastic_client,
                    query=query_without_keyword,
                    time_zone=os.environ.get("TZ", "Europe/Oslo"),
                    cursor=cursor,
                )

            for src_ip, dst_ip, bytes_in, bytes_out in search["rows"]:
                yield {
                    "src_ip": src_ip,
                    "dst_ip": dst_ip,
                    "bytes_in": bytes_in,
                    "bytes_out": bytes_out,
                }

            cursor = search.get("cursor")

            if not cursor:
                done = True

    def get_mapping(self, client: Elasticsearch) -> dict:
        return client.indices.get_mapping(self.elastic_index)
