import json
import pandas as pd

INPUT_FILES_PATH = "/tmp/"
OUTPUT_FILES_PATH = "/model/"


INPUT_FILE_BF_PROBABILITY = INPUT_FILES_PATH + "BF_Prob.csv"
INPUT_FILE_BF_TIME = INPUT_FILES_PATH + "BF_time.csv"

OUTPUT_FILE_BF_PROBABILITY = OUTPUT_FILES_PATH + "BF_Prob.csv"
OUTPUT_FILE_BF_TIME = OUTPUT_FILES_PATH + "BF_time.csv"


def main(uuid_file):
    with open(INPUT_FILES_PATH + uuid_file, "r") as data:
        Assets_UUIDs = json.load(data)

    Name_assets = list(Assets_UUIDs["Assets_UUID"].keys())
    UUID_assets = list(Assets_UUIDs["Assets_UUID"].values())

    # read matrices

    BFsMatrix_Prob = pd.read_csv(INPUT_FILE_BF_PROBABILITY, index_col=0)
    BFsMatrix_time = pd.read_csv(INPUT_FILE_BF_TIME, index_col=0)

    BFs_Matrix_Prob_columns = list(BFsMatrix_Prob.columns)
    BFs_Matrix_Prob_rows = list(BFsMatrix_Prob.index)

    BFs_Matrix_time_columns = list(BFsMatrix_time.columns)
    BFs_Matrix_time_rows = list(BFsMatrix_time.index)

    for i in range(len(BFs_Matrix_Prob_rows)):
        for j in range(len(Name_assets)):
            if BFs_Matrix_Prob_rows[i] == Name_assets[j]:
                BFs_Matrix_Prob_rows[i] = UUID_assets[Name_assets.index(Name_assets[j])]

    for i in range(len(BFs_Matrix_time_rows)):
        for j in range(len(Name_assets)):
            if BFs_Matrix_time_rows[i] == Name_assets[j]:
                BFs_Matrix_time_rows[i] = UUID_assets[Name_assets.index(Name_assets[j])]

    BFsMatrix_Prob = pd.DataFrame(
        BFsMatrix_Prob.values,
        columns=BFs_Matrix_Prob_columns,
        index=BFs_Matrix_Prob_rows,
    )
    BFsMatrix_time = pd.DataFrame(
        BFsMatrix_time.values,
        columns=BFs_Matrix_time_columns,
        index=BFs_Matrix_time_rows,
    )
    BFsMatrix_Prob.to_csv(OUTPUT_FILE_BF_PROBABILITY)
    BFsMatrix_time.to_csv(OUTPUT_FILE_BF_TIME)
