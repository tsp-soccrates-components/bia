import os

""" Configuration file for the Business Impact Analyser API. """
BIA_GUI_ADDRESS = os.environ.get("BIA_GUI_ADDRESS")

# Saving file config
SAVING_DIRECTORY = "/savedRequests"
PREFIX_FILENAME = SAVING_DIRECTORY + "/bia_apiRequest_"
SUFFIX_FILENAME = '.log'
