#!/usr/bin/env python3
""" Deploy an API to compute the Business Impact from a request of the Graph
Visualizer (coded Javascript).
The request is composed of a list of Node and a list of edges, from which it
builds adjacency matrices.
"""
from fastapi import FastAPI, Response, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.staticfiles import StaticFiles
from typing import List, Optional
import uvicorn

from app.bia_cortex_const import Assets, New_Assets
from app.bia_cortex_request import evalProbability, evalCriticalAssets, updateModel, AssetIDNotFound, evalHostIsolationDef
from app.bia_gui_backend import evalBusinessImpactFromGraph, evalCriticalTimeFromGraph, evalAssetsCriticalityFromGraph
from app.businessGraph_const import Edge, Node, NODES_EXAMPLE, EDGES_EXAMPLE, Coa, COA_EXAMPLE
from app.config import PREFIX_FILENAME, SUFFIX_FILENAME
from app.containmentDefense import ContainmentDefense
from app.gen_graph_from_json import generateGraphFromJson


# Parameters
NTIMES_MONTECARLO = 2000


# API initialisation
app = FastAPI()
app_title = "Business Impact Analyser API"
app = FastAPI(title=app_title, docs_url=None)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.mount("/static", StaticFiles(directory="app/static"), name="static")


@app.get("/")
async def main():
    return {"message": "This the Business Impact Analyser Python API"}


@app.get("/docs", include_in_schema=False)
async def get_docs():
    return get_swagger_ui_html(
        openapi_url=app.openapi_url,
        title=app.title + " - Swagger UI",
        swagger_js_url="/static/swagger-ui-bundle.js",
        swagger_css_url="/static/swagger-ui.css",
        swagger_favicon_url="/static/swagger_api_icon.png",
    )

########## Business Impact Analyser Cortex requests ###############################

@app.post("/probability", status_code=200)
async def getProbability(response: Response, Impacted_assets: List[Assets], business_name: Optional[list]=[],
                 attack_time: Optional[str]="2021-11-04T13:45:05.098Z"):
    # Form of the result: {"business_name": businessname, "impact_probability": impactprobability,
    #                    "impact_criticality": ImpactCriticality, "critical_time": criticaltime})
    try:
        result = evalProbability(Impacted_assets, business_name, attack_time)
    except AssetIDNotFound:
        response.status_code = status.HTTP_400_BAD_REQUEST
        print("assetID not existing")
        return "assetID not existing"
    except FileNotFoundError:
        response.status_code = status.HTTP_503_SERVICE_UNAVAILABLE
        print("Model not initialised")
        return "Model not initialised"
    return result

@app.post("/criticality")
async def getCriticalAssets(response: Response, threshold: Optional[float] = 0.66):
    try:
        result = evalCriticalAssets(threshold = threshold)
    except FileNotFoundError:
        response.status_code = status.HTTP_503_SERVICE_UNAVAILABLE
        print("Model not initialised")
        return "Model not initialised"
    return result


@app.post("/update")
async def updateBIAModel(asset:New_Assets):
    """Request from the Cortex Responder to update the BIA model."""
    output_message = updateModel(assets=asset)
    return output_message

########## Business Impact Analyser GUI requests ###############################

@app.post("/computeBusinessImpact_GUI")
async def computeBusinessImpact_GUI(nodes: List[Node]=NODES_EXAMPLE,
         edges: List[Edge]=EDGES_EXAMPLE):
    """ Compute the Business Impact for a graph from the BIA GUI. """
    try:
        list_bImpact = evalBusinessImpactFromGraph(nodes=nodes, edges=edges,
                                          ntimes_montecarlo=NTIMES_MONTECARLO)
    except FileNotFoundError:
        response.status_code = status.HTTP_503_SERVICE_UNAVAILABLE
        print("Model not initialised")
        return "Model not initialised"
    return list_bImpact


@app.post("/computeCriticalTime_GUI")
async def computeCriticalTime_GUI(nodes: List[Node]=NODES_EXAMPLE,
         edges: List[Edge]=EDGES_EXAMPLE):
    """ Compute the critical time for a graph from the BIA GUI. """
    path_value, path = evalCriticalTimeFromGraph(nodes=nodes, edges=edges)
    return (path_value, path)


@app.post("/computeCriticality_GUI")
async def computeCriticality_GUI(nodes: List[Node]=NODES_EXAMPLE,
         edges: List[Edge]=EDGES_EXAMPLE):
    """ Compute the Criticality on Asset for a graph from the BIA GUI. """
    list_criticality = evalAssetsCriticalityFromGraph(nodes=nodes, edges=edges)
    return list_criticality


@app.post("/getRequestGraph")
async def getRequestGraph(request_timestamp: int):
    """ Get request from the timestamp, and extract the associated graph. """
    file_name = PREFIX_FILENAME + str(int(request_timestamp)) + SUFFIX_FILENAME
    bia_graph = generateGraphFromJson(file_name)
    return bia_graph


########## Response Planner API requests #######################################

@app.post("/computeBI_Containment_CoA")
async def computeBI_Containment_CoA(defenseName: str, hosts_IPs: List[str]):
    print(f"BIA API evaluates the BI of the defense {defenseName} on {hosts_IPs}")
    if ContainmentDefense(defenseName).isHostIsolation():
        business_impact = evalHostIsolationDef(hosts_IPs)
    else:
        business_impact = -1
    return business_impact


if __name__ == "__main__":
    uvicorn.run(app, port=8000, log_level="info")
